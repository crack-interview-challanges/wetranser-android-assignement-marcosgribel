## GET STARTED

* [Download Android Studio](https://developer.android.com/studio)

* Checkout the Project

`git clone https://marcosgribel@bitbucket.org/crack-interview-challanges/wetranser-android-assignement-marcosgribel.git`

* ./gradlew clean build

* [Run on Device](https://developer.android.com/studio/run/emulator)​


## The Project

The project leverages on several modern approaches to build Android applications with:

* Kotlin ❤️
* MVVM
* Clean Architecture
* Unidirectional Data Flow
* Powered by Android JetPack
* Ready Easy code modularization
* Material Design guideline
* Unit Tests ❤️️❤️️
* Neat Packing Structure
* Coroutines & Flow


**Libraries Highlight**

JetPack Compose, Dagger/Hilt, Retrofit, Google Accompanist, Mockk

## WHAT’s NEXT
Due to the time constraints, I decided to keep the scope of the project small and focused more on the areas of Architecture, Clean Code, and Testability. With that in mind, there are some possible areas of improvement that I would like to highlight

### Cache

Since the api data is static for listing and booking a room, a cache strategy would be a nice approach to make this feature live. A local cache would mainly store the booked rooms and merge with the results from the server.

### Error Handling
Currently the app handles general errors, however, thinking of providing a better feedback and experience to the user a more specific error handler could be implemented and show a different UI.
### Modularisation
Although this app is small enough to not need a modularisation, it is always something to keep in mind assuming that all projects should be ready to scale. I have already adopted a neat package structure following some concept of Clean Architecture thinking of a possibile modularisation in the future.
Testing
Last but not least..
Although there isn’t much going on in the app, there is always room to improve and test different scenarios and I would add more test cases accordingly.

## NOTES
I took the challenge as an opportunity to experiment with the latest version of a few libraries and you might find a few of them annotated with @Experimental, however, bear in mind I would be careful in using experimental libraries in an enterprise application.

## Author
Marcos Gribel

[Email](gribel.marcos@gmail.com)

[LinkedIn](https://www.linkedin.com/in/marcosgribel/)
