package com.marcosgribel.wetransfer.presentation.viemodel

import app.cash.turbine.test
import com.marcosgribel.wetransfer.data.api.model.ResponseRoom
import com.marcosgribel.wetransfer.data.entities.Result
import com.marcosgribel.wetransfer.domain.usecase.GetRoomListUseCase
import com.marcosgribel.wetransfer.presentation.viemodel.RoomListScreenViewModel.UiEvent
import com.marcosgribel.wetransfer.presentation.viemodel.RoomListScreenViewModel.UiEvent.OnDisplayData
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class RoomListScreenViewModelTest {

    private val testDispatcher = TestCoroutineDispatcher()
    private val useCaseMock = mockk<GetRoomListUseCase>()

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when create ViewModel then emit OnDisplayData`() = runTest {
        // Given
        coEvery {
            useCaseMock.invoke()
        } returns flowOf(Result.Success(ResponseRoom(emptyList())))

        // When
        val viewModel = RoomListScreenViewModel(useCaseMock)

        viewModel.uiEventState.test {
            Assert.assertEquals(
                OnDisplayData(emptyList()),
                awaitItem()
            )
        }

        // Then
        coVerify(exactly = 1) { useCaseMock.invoke() }
    }

    @Test
    fun `when fetch failed then emit OnError event`() = testDispatcher.runBlockingTest {
        // Given
        val msgMock = "Error Mock Message"
        val exception = mockk<Exception>(msgMock)
        every { exception.message } returns  msgMock
        coEvery { useCaseMock.invoke() } returns flowOf(Result.Error(exception))

        // When
        val viewModel = RoomListScreenViewModel(useCaseMock)

        // Then
        viewModel.uiEventState.test {
            Assert.assertEquals(
                UiEvent.OnError(msgMock),
                awaitItem()
            )
        }

        coVerify(exactly = 1) { useCaseMock.invoke() }
    }
}