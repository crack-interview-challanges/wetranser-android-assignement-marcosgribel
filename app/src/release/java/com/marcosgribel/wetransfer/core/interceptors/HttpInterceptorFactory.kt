package com.marcosgribel.wetransfer.core.interceptors

import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

object HttpInterceptorFactory {

    val interceptors: List<Interceptor> = listOf(httpLoggingInterceptor())

    private fun httpLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.NONE
    }

}