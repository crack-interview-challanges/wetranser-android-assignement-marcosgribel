package com.marcosgribel.wetransfer.core.di

import com.marcosgribel.wetransfer.data.api.RoomEndPoint
import com.marcosgribel.wetransfer.data.datasource.RoomService
import com.marcosgribel.wetransfer.data.datasource.ServiceManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ServiceModule {

    @Singleton
    @Provides
    fun provideServiceManager(retrofit: Retrofit) = ServiceManager(retrofit)

    @Singleton
    @Provides
    fun provideRoomEndPoint(retrofit: Retrofit): RoomEndPoint =
        retrofit.create(RoomEndPoint::class.java)

    @Singleton
    @Provides
    fun provideRoomService(
        serviceManager: ServiceManager,
        endPoint: RoomEndPoint
    ): RoomService = RoomService(
        serviceManager = serviceManager,
        endPoint = endPoint
    )

}