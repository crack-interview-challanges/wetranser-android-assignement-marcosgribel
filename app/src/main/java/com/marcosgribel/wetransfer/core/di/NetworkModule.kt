package com.marcosgribel.wetransfer.core.di

import com.marcosgribel.wetransfer.BuildConfig
import com.marcosgribel.wetransfer.core.OkHttpClientConfig
import com.marcosgribel.wetransfer.core.RetrofitConfig
import com.marcosgribel.wetransfer.core.interceptors.HttpInterceptorFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient = OkHttpClientConfig(
        interceptors = HttpInterceptorFactory.interceptors
    ).invoke()

    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit = RetrofitConfig(
        baseUrl = BuildConfig.BASE_API_URL,
        okHttpClient = okHttpClient,
    ).invoke()

}