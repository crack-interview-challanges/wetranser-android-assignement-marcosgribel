package com.marcosgribel.wetransfer.core

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class OkHttpClientConfig @Inject constructor(
    private val interceptors: List<Interceptor>,
) {
    operator fun invoke(): OkHttpClient {
        val client = OkHttpClient.Builder()
        interceptors.forEach { interceptor ->
            client.addInterceptor(interceptor)
        }
        return client.build()
    }
}

class RetrofitConfig @Inject constructor(
    private val baseUrl: String,
    private val okHttpClient: OkHttpClient,
    private val converterFactory: GsonConverterFactory = GsonConverterFactory.create(),
) {
    operator fun invoke(): Retrofit = Retrofit.Builder()
        .addConverterFactory(converterFactory)
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .build()
}