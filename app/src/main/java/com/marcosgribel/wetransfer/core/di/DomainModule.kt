package com.marcosgribel.wetransfer.core.di

import com.marcosgribel.wetransfer.data.datasource.RoomService
import com.marcosgribel.wetransfer.domain.repository.RoomRepo
import com.marcosgribel.wetransfer.domain.usecase.GetRoomListUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DomainModule {

    @Singleton
    @Provides
    fun provideRoomsRepo(service: RoomService) = RoomRepo(
        service = service
    )

    @Singleton
    @Provides
    fun provideGetRoomsUseCase(repo: RoomRepo) = GetRoomListUseCase(
        roomRepo = repo,
        coroutineDispatcher = Dispatchers.IO
    )

}