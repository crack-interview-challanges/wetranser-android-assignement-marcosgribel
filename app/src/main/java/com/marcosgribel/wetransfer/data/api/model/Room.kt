package com.marcosgribel.wetransfer.data.api.model

import com.google.gson.annotations.Expose

data class Room(
    val name: String,
    val spots: Int = 0,
    val thumbnail: String,
    @Expose(deserialize = false) var booked: Boolean = false
) {
    override fun equals(other: Any?): Boolean {
        return other is Room && other.name == this.name
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}