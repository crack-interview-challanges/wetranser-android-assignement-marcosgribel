package com.marcosgribel.wetransfer.data.api

import com.marcosgribel.wetransfer.data.api.model.ResponseRoom
import retrofit2.Response
import retrofit2.http.GET

interface RoomEndPoint {

    @GET("rooms.json")
    suspend fun getRooms(): Response<ResponseRoom>

}
