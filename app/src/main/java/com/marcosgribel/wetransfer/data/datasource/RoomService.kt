package com.marcosgribel.wetransfer.data.datasource

import com.marcosgribel.wetransfer.data.api.RoomEndPoint
import javax.inject.Inject

class RoomService @Inject constructor(
    private val serviceManager: ServiceManager,
    private val endPoint: RoomEndPoint
) {

    suspend fun getRooms() = serviceManager.execute({ endPoint.getRooms() })

}