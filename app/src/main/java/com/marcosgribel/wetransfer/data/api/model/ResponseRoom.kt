package com.marcosgribel.wetransfer.data.api.model

data class ResponseRoom(
    val rooms: List<Room>
)