package com.marcosgribel.wetransfer.data.datasource

import com.marcosgribel.wetransfer.data.entities.Result
import com.marcosgribel.wetransfer.data.entities.Result.Success
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class ServiceManager @Inject constructor(
    private val retrofit: Retrofit
) {

    suspend fun <T> execute(
        apiCall: suspend () -> Response<T>,
        defaultErrorMessage: String = "Something went wrong!"
    ): Result<T> {
        return try {
            with(apiCall()) {
                if (isSuccessful) {
                    Success(this.body()!!)
                } else {
                    val converter = retrofit.responseBodyConverter<Error>(
                        Error::class.java,
                        arrayOfNulls(0)
                    )
                    val error = converter.convert(errorBody()!!)
                    Result.Error(error ?: Throwable(defaultErrorMessage))
                }
            }
        } catch (e: Throwable) {
            Result.Error(e)
        }
    }

}