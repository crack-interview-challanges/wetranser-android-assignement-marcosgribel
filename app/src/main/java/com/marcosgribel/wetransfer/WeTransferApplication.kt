package com.marcosgribel.wetransfer

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WeTransferApplication : Application() {
}