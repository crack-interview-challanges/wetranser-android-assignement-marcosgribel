package com.marcosgribel.wetransfer.domain.usecase

import com.marcosgribel.wetransfer.data.api.model.ResponseRoom
import com.marcosgribel.wetransfer.data.entities.Result
import com.marcosgribel.wetransfer.domain.repository.RoomRepo
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class GetRoomListUseCase @Inject constructor(
    private val roomRepo: RoomRepo,
    private val coroutineDispatcher: CoroutineDispatcher,
) {

    suspend fun invoke(): Flow<Result<ResponseRoom>> = flow {
        try {
            emit(roomRepo.getRooms())
        } catch (e: Exception) {
            emit(Result.Error(exception = e))
        }
    }.flowOn(coroutineDispatcher)

}