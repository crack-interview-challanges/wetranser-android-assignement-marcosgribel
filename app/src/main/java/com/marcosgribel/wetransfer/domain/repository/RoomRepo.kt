package com.marcosgribel.wetransfer.domain.repository

import com.marcosgribel.wetransfer.data.api.model.ResponseRoom
import com.marcosgribel.wetransfer.data.datasource.RoomService
import com.marcosgribel.wetransfer.data.entities.Result
import javax.inject.Inject

class RoomRepo @Inject constructor(
    private val service: RoomService,
) {

    suspend fun getRooms(): Result<ResponseRoom> = service.getRooms()

}