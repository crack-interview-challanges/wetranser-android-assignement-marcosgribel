package com.marcosgribel.wetransfer.presentation.ui.theme

import androidx.compose.ui.unit.dp

object Dimension {
    val xxSmall = 2.dp
    val xSmall = 4.dp
    val small = 8.dp
    val medium = 16.dp
    val large = 24.dp
    val xLarge = 32.dp
    val xxLarge = 48.dp
    val xxxLarge = 56.dp
}