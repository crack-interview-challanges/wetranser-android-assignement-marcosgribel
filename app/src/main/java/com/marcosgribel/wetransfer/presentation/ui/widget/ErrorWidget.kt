package com.marcosgribel.wetransfer.presentation.ui.widget

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.marcosgribel.wetransfer.R
import com.marcosgribel.wetransfer.presentation.ui.theme.Dimension

@Composable
fun ErrorWidget(
    onTryAgainClick: () -> Unit
) {
    Column(
        modifier = androidx.compose.ui.Modifier
            .padding(Dimension.medium)
            .fillMaxSize(),
    ) {
        Column(
            modifier = Modifier.weight(2F),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Icon(
                Icons.Outlined.Close,
                modifier = Modifier
                    .size(150.dp)
                    .padding(vertical = Dimension.medium),
                contentDescription = stringResource(R.string.localized_description)
            )
            Text(
                modifier = Modifier.padding(vertical = Dimension.medium),
                text = stringResource(R.string.body_something_went_wrong),
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Normal,
                fontSize = 24.sp
            )
        }
        Button(
            modifier = Modifier
                .fillMaxWidth()
                .size(Dimension.xxxLarge)
                .padding(horizontal = Dimension.medium),
            onClick = onTryAgainClick
        ) {
            Text(
                text = stringResource(R.string.btn_try_again),
                color = Color.White
            )
        }
    }
}
