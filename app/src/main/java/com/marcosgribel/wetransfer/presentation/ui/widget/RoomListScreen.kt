package com.marcosgribel.wetransfer.presentation.ui.widget

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberImagePainter
import com.marcosgribel.wetransfer.R
import com.marcosgribel.wetransfer.data.api.model.Room
import com.marcosgribel.wetransfer.presentation.ui.theme.Dimension
import com.marcosgribel.wetransfer.presentation.viemodel.RoomListScreenViewModel
import com.marcosgribel.wetransfer.presentation.viemodel.RoomListScreenViewModel.UiEvent
import com.marcosgribel.wetransfer.presentation.viemodel.RoomListScreenViewModel.UiEvent.OnDisplayData

@Composable
fun RoomListScreen(
    viewModel: RoomListScreenViewModel = hiltViewModel()
) {
    val state: State<UiEvent> = viewModel.uiEventState.collectAsState(UiEvent.OnLoading)

    Scaffold(
        modifier = Modifier,
    ) {
        with(state.value) {
            when (this) {
                is OnDisplayData -> {
                    RoomListWidget(rooms = this.results) {}
                }
                is UiEvent.OnLoading -> LoadingWidget()
                is UiEvent.OnError -> ErrorWidget {
                    viewModel.perform(RoomListScreenViewModel.UiAction.OnFetch)
                }
            }
        }
    }
}

@Composable
fun RoomListWidget(
    rooms: List<Room>,
    onBookRoomClick: (room: Room) -> Unit
) {
    LazyColumn {
        items(
            items = rooms,
            itemContent = {
                RoomRowWidget(
                    thumbnail = it.thumbnail,
                    title = it.name,
                    quantity = it.spots,
                ) { onBookRoomClick(it) }
            }
        )
    }
}

@Composable
fun RoomRowWidget(
    thumbnail: String,
    title: String,
    quantity: Int,
    onBtnBookClick: () -> Unit = {}
) {
    Box(
        modifier = Modifier.padding(Dimension.medium)
    ) {
        Column {
            Card {
                Image(
                    painter = rememberImagePainter(thumbnail),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .fillMaxWidth()
                        .size(164.dp)
                )
            }
            Row(
                modifier = Modifier
                    .padding(vertical = Dimension.small)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
            ) {
                Column {
                    Text(
                        text = title,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        text = LocalContext.current.resources.getQuantityString(
                            R.plurals.room_availability_quantity, quantity, quantity
                        ), color = MaterialTheme.colors.primary
                    )
                }
                Button(
                    onClick = onBtnBookClick,
                ) {
                    Text(
                        stringResource(id = R.string.btn_book),
                        modifier = Modifier.padding(horizontal = Dimension.small)
                    )
                }
            }
        }
    }
}
