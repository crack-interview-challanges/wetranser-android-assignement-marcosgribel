package com.marcosgribel.wetransfer.presentation.viemodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.marcosgribel.wetransfer.data.api.model.Room
import com.marcosgribel.wetransfer.data.entities.Result
import com.marcosgribel.wetransfer.domain.usecase.GetRoomListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RoomListScreenViewModel @Inject constructor(
    private val getRoomListUseCase: GetRoomListUseCase
) : ViewModel() {

    sealed class UiAction {
        object OnFetch : UiAction()
    }

    sealed class UiEvent {
        object OnLoading : UiEvent()
        data class OnDisplayData(val results: List<Room>) : UiEvent()
        data class OnError(val message: String?) : UiEvent()
    }

    private val _uiEventState = MutableStateFlow<UiEvent>(UiEvent.OnLoading)
    val uiEventState: Flow<UiEvent> = _uiEventState

    init {
        perform(UiAction.OnFetch)
    }

    fun perform(action: UiAction) {
        when (action) {
            is UiAction.OnFetch -> fetch()

        }
    }

    private fun fetch() {
        viewModelScope.launch {
            getRoomListUseCase.invoke()
                .onStart {
                    _uiEventState.value = UiEvent.OnLoading
                }
                .collect { result ->
                    _uiEventState.value = when (result) {
                        is Result.Success -> {
                            UiEvent.OnDisplayData(results = result.data.rooms)
                        }
                        is Result.Error -> {
                            UiEvent.OnError(result.exception?.message)
                        }
                    }
                }
        }

    }

}


